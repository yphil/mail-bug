;;; mail-bug.el --- A purely IMAP based email client for Emacs.

;; Copyright (C) 2012, 2017 Philippe Coatmeur
;; URL: https://bitbucket.org/yassinphilip/mail-bug

;; Author: Yassin Philip <xaccrocheur@gmail.com>
;; Maintainer: Yassin Philip <xaccrocheur@gmail.com>
;; Keywords: mail
;; Version: 0.8.0
;; URL: https://bitbucket.org/yassinphilip/mail-bug
;; License: GPLv3
;; Codeship-key: 2c0e30d0-66d5-0135-e9a2-16754ac7c510
;; Codeship-prj: 240973

;; This file is NOT part of GNU Emacs.

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2, or (at your option)
;; any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program; see the file COPYING.  If not, write to the
;; Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;; Boston, MA 02111-1307, USA.

;;; Commentary:

;; Mail Bug is pure IMAP online-only mail client. I use it to read a
;; quick mail on my main account. It is probably **not** your new main
;; email client.
;;
;; For starters, it only supports/displays *one* email account at a
;; time. And, it's as fast as your internet connection, So that's
;; that, too.
;;
;; It has been improved lately in the area of:
;;
;; - Authentication & security
;; - Subtle ergonomics
;; - Customization
;; - Charset decoding
;; - Message management
;;     - Moving
;;     - Spam flagging
;; - Attachment handling
;; - HTML rendering
;; - Replying & sending
;; - SMTP handling
;;
;; ## Installation
;; - Put/clone it in the `load-path` and then either:
;;     - Use `customize-group RET mail-bug RET`
;;     - Or set it up manually:
;; ```elisp
;; (require 'mail-bug)

;; (setq mbug-imap-host-name "imap.hostname")
;; (setq mbug-smtp-host-name "smtp.hostname")
;; (setq mbug-username "username")
;; ```
;; - Create a `~/.authinfo.gpg` file with a line for your IMAP server:
;; `machine YOURIMAPSERVER login YOURUSERNAME password YOURPASSWORD`
;;     - If your "machine" (server) is Gmail, you must use an "app password", [generate
;; one](https://myaccount.google.com/apppasswords) at google's.
;; - The SMTP entry will be created when you send your first mail.

;; ## Usage
;; Run `mail-bug` to connect ; The keys/commands available to
;; read, send and manage messages are detailed below.

;;; Code:

(require 'imap)
(require 'qp)
(require 'timezone)
(require 'message)
(eval-when-compile
  (require 'cl))
(require 'dbus)
(require 'auth-source)
(require 'starttls)

(setq auth-source-debug 'trivia)

(load-library "smtpmail")
(defconst imap-log-buffer "*imap-log*")
(defconst imap-debug-buffer "*imap-debug*")

(when imap-debug            ; (untrace-all)
  (require 'trace)
  (buffer-disable-undo (get-buffer-create imap-debug-buffer))
  (mapc (lambda (f) (trace-function-background f imap-debug-buffer))
        '(imap-utf7-encode
          imap-utf7-decode
          imap-error-text
          imap-kerberos4s-p
          imap-kerberos4-open
          imap-network-p
          imap-network-open
          imap-interactive-login
          imap-kerberos4a-p
          imap-kerberos4-auth
          imap-cram-md5-p
          imap-cram-md5-auth
          imap-login-p
          imap-login-auth
          imap-anonymous-p
          imap-anonymous-auth
          imap-open-1
          imap-open
          imap-opened
          imap-ping-server
          imap-authenticate
          imap-close
          imap-capability
          imap-namespace
          imap-send-command-wait
          imap-mailbox-put
          imap-mailbox-get
          imap-mailbox-map-1
          imap-mailbox-map
          imap-current-mailbox
          imap-current-mailbox-p-1
          imap-current-mailbox-p
          imap-mailbox-select-1
          imap-mailbox-select
          imap-mailbox-examine-1
          imap-mailbox-examine
          imap-mailbox-unselect
          imap-mailbox-expunge
          imap-mailbox-close
          imap-mailbox-create-1
          imap-mailbox-create
          imap-mailbox-delete
          imap-mailbox-rename
          imap-mailbox-lsub
          imap-mailbox-list
          imap-mailbox-subscribe
          imap-mailbox-unsubscribe
          imap-mailbox-status
          imap-mailbox-acl-get
          imap-mailbox-acl-set
          imap-mailbox-acl-delete
          imap-current-message
          imap-list-to-message-set
          imap-fetch-asynch
          imap-fetch
          imap-fetch-safe
          imap-message-put
          imap-message-get
          imap-message-map
          imap-search
          imap-message-flag-permanent-p
          imap-message-flags-set
          imap-message-flags-del
          imap-message-flags-add
          imap-message-copyuid-1
          imap-message-copyuid
          imap-message-copy
          imap-message-appenduid-1
          imap-message-appenduid
          imap-message-append
          imap-body-lines
          imap-envelope-from
          imap-send-command-1
          imap-send-command
          imap-wait-for-tag
          imap-sentinel
          imap-find-next-line
          imap-arrival-filter
          imap-parse-greeting
          imap-parse-response
          imap-parse-resp-text
          imap-parse-resp-text-code
          imap-parse-data-list
          imap-parse-fetch
          imap-parse-status
          imap-parse-acl
          imap-parse-flag-list
          imap-parse-envelope
          imap-parse-body-extension
          imap-parse-body)))

(defun mbug-eval-smtp ()
  "Set up some vars."
  (setq
   smtpmail-default-smtp-server mbug-smtp-host-name
   smtpmail-debug-info t
   smtpmail-smtp-service 587 ;; inoperant
   smtpmail-starttls-credentials '((mbug-smtp-host-name 587 nil nil))
   smtpmail-auth-credentials (expand-file-name "~/.authinfo.gpg")
   smtpmail-smtp-server mbug-smtp-host-name))

(defcustom mbug-username ""
  "The username (login)."
  :type '(string)
  :group 'mail-bug)

(defcustom mbug-imap-host-name ""
  "The name of the server to connect to."
  :type '(string)
  :group 'mail-bug)

(defcustom mbug-smtp-host-name ""
  "The name of the SMTP server to connect to."
  :type '(string)
  :group 'mail-bug)

(defvar mbug-imap-port 993
  "The imap server port.")

(defvar mbug-password nil
  "The user's password.")

(defvar mbug-dir (concat (file-name-directory (or load-file-name (buffer-file-name))) "assets/")
  "The mail-bug dir.")

;; Customizations (Don't touch this, M-x customize-group "mail-bug" RET instead)
(defgroup mail-bug nil
  "Mail-bug - A lightweight Mail User Agent for GNU Emacs."
  :group 'applications)

(defgroup mail-bug-interface nil
  "Faces for mails in Mail-bug summary buffer.
The faces inherit from emacs defult faces, so it's OK if you do nothing here."
  :group 'mail-bug)

(defcustom mbug-modal 't
  "Should the message open in a preview windowpane?
NOTE: This is only relevant in windowed (ie not console) mode."
  :type '(boolean)
  :group 'mail-bug-interface)

(defcustom mbug-dedicated nil
  "Should the message open in a dedicated windowpane?
Dedicated windows are intangible.
NOTE: tabbar don't show in dedicated windows.
NOTE: This is only relevant in windowed (ie not console) mode."
  :type '(boolean)
  :group 'mail-bug-interface)

(defcustom mbug-inline-images 't
  "Should the images be displayed directly in the message windowpane?"
  :type '(boolean)
  :group 'mail-bug-interface)

(defcustom mbug-splash 't
  "Should something move at each launch?"
  :type '(boolean)
  :group 'mail-bug-interface)

(defcustom mbug-bug 't
  "Should the user be bugged with new msgs?
This can be somewhat blocking on slow imap servers/connections"
  :type '(boolean)
  :group 'mail-bug-interface)

(defcustom mbug-short-headers 't
  "Should the headers show only the standard values?"
  :type '(boolean)
  :group 'mail-bug-interface)

(defcustom mbug-bcc-to-sender 't
  "Should the sender be sent copies of all mails?"
  :type '(boolean)
  :group 'mail-bug)

(defcustom mbug-initial-folder-name ""
  "The name to popup when selecting a target folder for moves."
  :type '(string)
  :group 'mail-bug)

(defcustom mbug-trash-folder-name "Trash"
  "The folder name of the folder to save deleted emails in."
  :type '(string)
  :group 'mail-bug)

(defcustom mbug-spam-folder-name "Spam"
  "The folder name of the folder to save spam messages in."
  :type '(string)
  :group 'mail-bug)


;; Notification
(defcustom mail-bug-icon
  (when (image-type-available-p 'xpm)
    `(image :type xpm
            :file ,(concat mbug-dir "greenbug.xpm")
            :ascent center))
  "Icon for the first account.
Must be an XPM (use Gimp)."
  :group 'mail-bug-interface)

(defconst mail-bug-logo
  (if (and window-system
           mail-bug-icon)
      (apply 'propertize " " `(display ,mail-bug-icon))
    mbug-imap-host-name))

(defcustom mbug-new-mail-icon (concat mbug-dir "new_mail.png")
  "Icon for new mail notification.
PNG works."
  :type 'string
  :group 'mail-bug-interface)

(defcustom mbug-new-mail-sound (concat mbug-dir "new_mail.wav")
  "Sound for new mail notification.
Wav only."
  :type 'string
  :group 'mail-bug-interface)

;; (message mbug-new-mail-sound)

(defcustom mbug-timer-seconds 320
  "Interval(in seconds) for mail check."
  :type 'number
  :group 'mail-bug-interface)

(defgroup mail-bug-faces nil
  "Faces for folders and message contents."
  :group 'mail-bug-interface)

;; Faces
(defface mbug-face-folder
  `((((class color) (background dark))
     (:weight bold))
    (((class color) (background light))
     (:weight bold))
    (((type tty) (class color))
     (:weight bold))
    (((type tty) (class mono))
     (:weight bold))
    (t (:weight bold)))
  "Basic face for IMAP directories."
  :group 'mail-bug-faces)

(defface hide-region-after-string-face
  '((t (:inherit region)))
  "Face for the togglable hidden elements such as headers.")

(defface mbug-px-face-marked
  '((t (:inherit secondary-selection)))
  "Basic face for deleted mails."
  :group 'mail-bug-faces)

(defvar hide-region-before-string "[(h)eaders"
  "String to mark the beginning of an invisible region.
This string is not really placed in the text, it is just shown in the overlay.")

(defvar hide-region-after-string "]"
  "String to mark the end of an invisible region.")

(defvar hide-region-propertize-markers t
  "If non-nil, add text properties to the region markers.")

(defface hide-region-before-string-face
  '((t (:inherit region)))
  "Face for the header-hiding string.")

(defface hide-region-after-string-face
  '((t (:inherit region)))
  "Face for the after string.")

(defface mail-bug-toggle-headers-face
  '((t (:inherit region)))
  "Face for the header-hiding string.")

(defvar hide-region-overlays nil
  "Variable to store the regions we put an overlay on.")

(defvar mbug-host nil
  "The imap server.")

(defvar mbug-connection nil
  "The imap connection is a process bound buffer.")

(defvar mbug-mode-initialized-p nil
  "Is the mode initialized already?")

(defvar mbug-mode-hook nil
  "The mode hooks.")

(defvar mbug-mode-map nil
  "The mode keymap.")

(defvar mbug-message-keymap-initializedp nil
  "Is the message view mode map initialized yet?")

(defvar mbug-message-mode-hook nil
  "The mode hooks.")

(defvar mbug-folder-list nil
  "The cached list of folders.")

(defvar mbug-smart-folder-list nil
  "The cached alist (NAME . PATH) of folders.")

(defvar mbug-folder-history nil
  "The history list for the message moves.")

(defvar mbug-unread-mails nil)

(with-no-warnings
  (defvar openingp ())
  (defvar init 't)
  (defvar mbug-sorted-raw-folder-list)
  (defvar mbug-timer)
  (defvar mbug-message-text-end-of-headers)
  (defvar part)
  (defvar mailcap-ext-pattern)
  (defvar name)
  (defvar uid)
  (defvar imap-con)
  (defvar mailcap-viewer)
  (defvar buffer)
  (defvar mbug-px-face-marked)
  (defvar atom-list)
  (defvar text-chars)
  (defvar text-trans)
  (defvar folder-icon)
  (defvar newmail))


(defvar mbug-advertised-mails '())
(defvar mbug-to-be-advertised-mails '())


(defconst myregexp-html-start
  (rx
   (minimal-match
    (or
     "<!DOCTYPE"
     "<html"))))

(defconst myregexp-html-end
  (rx
   "</html"))

;; Draft and rogue message buffers
(add-to-list 'auto-mode-alist '("*message*" . message-mode))

(setq imap-log t)

(defun mbug-toggle-imap-logging ()
  "Toggle the imap logging."
  (interactive)
  (if imap-log
      (setq imap-log nil)
    (setq imap-log (get-buffer-create "mbug-log"))))

;; This is a function pinched from gnus-sum
(defun mbug-trim (str)
  "Remove excessive whitespace from STR."
  (let ((mystr str))
    ;; Multiple spaces.
    (while (string-match "[ \t][ \t]+" mystr)
      (setq mystr (concat (substring mystr 0 (match-beginning 0))
                          " " (substring mystr (match-end 0)))))
    ;; Leading spaces.
    (when (string-match "^[ \t]+" mystr)
      (setq mystr (concat
                   (substring mystr 0 (match-beginning 0))
                   (substring mystr (match-end 0)))))
    ;; Trailing spaces.
    (when (string-match "[:space:]$" mystr)
      (setq mystr (concat (substring mystr 0 (match-beginning 0)))))
    mystr))

(defun mbug-kill-buffer-hook ()
  "Ensure the IMAP connection is logged out when the buffer dies."
  (mbug-logout)
  (global-unset-key [menu-bar mbug-menu])
  (setq init 't))

(defun mbug-ensure-connected ()
  "Get a connection to the mail store."
  (if (imap-opened mbug-connection)
      mbug-connection
    ;; Else create the connection
    (if (not mbug-host)
        (if (not (equal mbug-imap-host-name ""))
            (setq mbug-host mbug-imap-host-name)
          (setq mbug-host (read-from-minibuffer "Host: "))))

    (setq mbug-connection (imap-open mbug-host mbug-imap-port 'tls 'login))
    (assert mbug-connection nil "the imap connection could not be opened")

    (condition-case nil
        (progn

          (setq mbug-login
                (car
                 (mbug-credentials
                  mbug-imap-host-name mbug-imap-port)))
          
          (setq mbug-password
                (cadr
                 (mbug-credentials
                  mbug-imap-host-name mbug-imap-port)))

          (imap-authenticate
           mbug-username
           mbug-password
           mbug-connection)
          ;; Initialize the connection by listing all mailboxes.
          (imap-mailbox-list "*" "" "." mbug-connection))
      (error "Ooops" nil))))

(defun mbug-credentials (address ports)
  (let* ((auth-source-creation-prompts
          '((user  . "IMAP user at %h: ")
            (secret . "IMAP password for %u@%h: ")))
         (found (nth 0 (auth-source-search :max 1
                                           :host address
                                           :port ports
                                           :require '(:user :secret)
                                           :create t))))
    (if found
        (list (plist-get found :user)
              (let ((secret (plist-get found :secret)))
                (if (functionp secret)
                    (funcall secret)
                  secret)))
      nil)))

(defun mbug-count-occurrences (regexp str)
  "How many REGEXP in STR."
  (loop with start = 0
        for count from 0
        while (string-match regexp str start)
        do (setq start (match-end 0))
        finally return count))

(defun mbug-refresh-folder-list ()
  "Refresh the list of folders available from the imap server.
Folder name/path alist that looks like this:
  \((\"One\" . \"Top/One\")
    (\"INBOX\" . \"INBOX\")) to allow navigation."
  (imap-mailbox-list "*" "" "." mbug-connection)

  (setq mbug-sorted-raw-folder-list
        (sort
         (imap-mailbox-map
          (lambda (folder-name)
            folder-name)  mbug-connection) 'string<))
  (setq mbug-smart-folder-list
        (mapcar
         (lambda (folder-name)
           (cons (replace-regexp-in-string ".*/" "" folder-name) folder-name))
         mbug-sorted-raw-folder-list)))

(defun mbug-field-format (str width &optional padding-only)
  "Return a string STR padded or truncated to fit in the field WIDTH.
If PADDING-ONLY is non-nil then truncation will not be performed."
  (if (> (length str) width)
      (if (not padding-only)
          (concat (substring str 0 (- width 3)) "...")
        str)
    (concat str (make-string (- width (length str)) ?\ ))))


(defun mbug-from-format (from-addr)
  "Return the best string representing the FROM-ADDR.
The supplied address is a vector of 3 different address types.
imap.el returns the from address in element 0, but it's more reliable
to concatentate the domain (element 3) and the username (element 2)"
  (let ((friendly-name (elt from-addr 0))
        (other-name (elt from-addr 1))
        (smtp-addr (elt from-addr 2))
        (domain (elt from-addr 3)))
    ;;(if (gethash friendly-name mbug-friends)
    ;;friendly-name
    (concat smtp-addr "@" domain)));;)


(defun mbug-date-format (date-string)
  "Return the best string representation of the supplied DATE-STRING.
The timezone package is used to parse the string."
  (let ((date-struct (timezone-parse-date date-string)))
    ;; (print date-string (get-buffer "*scratch*"))
    (concat
     ;; Year
     (elt date-struct 0)
     "-"
     ;; Handle month padding
     (let ((month (elt date-struct 1)))
       (if (< (length month) 2)
           (concat "0" month)
         month))
     "-"
     ;; Handle day padding
     (let ((day (elt date-struct 2)))
       (if (< (length day) 2)
           (concat "0" day)
         day))
     " "
     ;; Time
     (elt date-struct 3) " ")))

(defun mbug-flag-p (uid flag)
  "Return true if UID has FLAG."
  (let ((flag-list (imap-message-get uid 'FLAGS mbug-connection))
        (this-fn
         (lambda (flag-list fn)
           (if (listp flag-list)
               (let ((this-flag (car flag-list)))
                 (if (and (stringp this-flag)
                          (string= flag this-flag))
                     't
                   (if (cdr flag-list)
                       (funcall fn (cdr flag-list) fn)
                     nil)))
             nil))))
    (funcall this-fn flag-list this-fn)))

(defun mbug-has-recent-p (folder)
  "Has the specified FOLDER got a recent marker?"
  (catch 'exit-recur
    (mapc (lambda (item)
            (if (equal (upcase-initials item) "\\Marked")
                (throw 'exit-recur 't)))
          (imap-mailbox-get 'list-flags folder mbug-connection))
    nil))

(defun part-num-to-str (super-part part)
  "Convert a SUPER-PART number to a compound string PART."
  (if super-part
      (format "%s.%s" super-part part)
    (format "%s" part)))

(defun ext-parse (bs lst)
  "Parse the body struct BS LST extension data."
  (unless (eq 'NIL (elt lst 0))
    (nconc bs (list (cons 'body (list (elt lst 0))))))
  (unless (eq 'NIL (elt lst 1))
    (nconc bs (list (cons 'disposition (list (elt lst 1))))))
  (unless (eq 'NIL (elt lst 3))
    (nconc bs (list (cons 'transfer-encoding (list (elt lst 3))))))
  ;; We need more statements here to put all the extension data into the alist
  bs)

(defun mbug-parse-bs (lst &optional super-part)
  "Turn a mime structure LST into an alist.
The keys of the alist are tags for different parts (SUPER-PART)
of the message, for example 'type is the mime type.  Multipart
messages are coded exactly the same except they have each subpart
in the alist as well.  Each subpart is keyed by its part id (as a
string)."

  ;; (message "lst: %S" lst)
  ;; Main func.
  (let ((bs (list '()))
        (part 1)
        (el (car lst)))
    (while (listp el)
      (let ((part-str (part-num-to-str super-part part)))
        (nconc bs (list (cons part-str (mbug-parse-bs el part-str))))
        (setq part (+ 1 part))
        (setq lst (cdr lst))
        (setq el (car lst))))

    ;; el now points to the mime type of the overall part
    (if (not (listp (cadr lst)))
        ;; This is a simple type
        (progn
          (nconc bs (list (cons 'type (list (cons el (cadr lst))))))
          (ext-parse bs (cddr lst))
          )
      ;; This is a multipart
      (progn
        (nconc bs (list (cons 'type el)))
        (ext-parse bs (cdr lst))
        ))
    (cdr bs)))

(defun mbug-part-list-assoc (key value malist)
  "Find the specified KEY/VALUE pair in the MALIST.
An malist is a Multi Association LIST: a list of alists."
  (let ((found (catch 'found
                 (mapc (lambda (alist)
                         (if (equal value (cdr (assoc key alist)))
                             (throw 'found alist))) malist))))
    found))

(defun mbug-char-repeat (str n)
  "Repeat string STR N times."
  (let ((retval ""))
    (dotimes (i n)
      (setq retval (concat retval str)))
    retval))


(defun mbug-splash-it ()
  "Move things around the screen."
  (animate-string "mail-bug 0.1.2β "
                  (/ (cadddr (window-edges)) 2)
                  (- (/ (caddr (window-edges)) 2) 15)))

(defun mbug-timer-start ()
  "Init."
  (interactive)
  (message "Mbug-timer started (stop it with `mbug-timer-kill')")
  (setq mbug-timer (run-with-timer 15
                                   mbug-timer-seconds
                                   'mbug-recount)))


(defun mbug-timer-kill ()
  "Stop everything."
  (interactive)
  (cancel-timer mbug-timer))


;; The initializing proc.
;;;###autoload
(defun mail-bug (&optional host-name tcp-port)
  "Open the imap server and get a folder list.
With a non-nil prefix argument the imap server HOST-NAME is requested.
This means you can have multiple mbug sessions in one Emacs session.
TCP-PORT is optional."
  (interactive
   (if current-prefix-arg
       (let ((host-str (read-from-minibuffer "Imap server host name: ")))
         (string-match "\\(.+\\) \\([0-9]+\\)" host-str 0)
         (list (if (not (match-string 1 host-str))
                   "localhost"
                 (match-string 1 host-str))
               (if (not (match-string 2 host-str))
                   993
                 (string-to-number (match-string 2 host-str)))))))

  ;; Setup buffer.
  (let ((folder-buffer (get-buffer-create
                        (concat "mail-bug"
                                (if host-name
                                    (concat host-name ":" (number-to-string tcp-port)))))))
    (switch-to-buffer folder-buffer)
    ;; (newline)

    (linum-mode -1)
    (if mbug-splash
        (mbug-splash-it))

    ;; (beginning-of-buffer)
    (if (not mbug-mode-initialized-p)
        (progn
          (mbug-mode)
          ;; If a host has been specified then make the host name local.
          (if host-name
              (progn
                (make-local-variable 'mbug-host)
                (setq mbug-host host-name)
                (make-local-variable 'mbug-imap-port)
                (setq mbug-imap-port tcp-port)))))
    (mbug-redraw)
    (goto-char (point-min))))


(defun mbug-menu ()
  "Create the Mail-bug menu."
  (define-key-after
    global-map
    [menu-bar mbug-menu]
    (cons "Mail-bug" (make-sparse-keymap "hoot hoot"))
    'tools )

  (define-key
    global-map
    [menu-bar mbug-menu prefs]
    '("Prefs" . (lambda () (interactive) (customize-group 'mail-bug))))

  (define-key
    global-map
    [menu-bar mbug-menu help]
    '("Help" . (lambda () (interactive) (describe-mode (get-buffer "mail-bug"))))))

(add-hook 'mbug-mode-hook
          (lambda ()
            (toggle-truncate-lines 1)
            (linum-mode 0)
            (hl-line-mode t)))

(add-hook 'mbug-message-mode-hook
          (lambda ()
            (goto-address-mode t)))


(add-hook 'mbug-mode-hook 'my-inhibit-global-linum-mode)

(defun my-inhibit-global-linum-mode ()
  "Counter-act `global-linum-mode'."
  (add-hook 'after-change-major-mode-hook
            (lambda () (linum-mode 0))
            :append :local))

(define-derived-mode mbug-mode fundamental-mode "Mail"

  "The \"list\" mode.

Every command act on the object (folder or message) under point,
or on lines selected in the normal fashion.

Available commands in the message/folder list:

\\{mbug-mode-map}"
  (kill-all-local-variables)
  (unless mbug-mode-map
    (setq mbug-mode-map (make-sparse-keymap))

    (define-key mbug-mode-map "\r" 'mbug-open)
    (define-key mbug-mode-map "+" 'mbug-create-folder)
    ;; (define-key mbug-mode-map "/" 'isearch-forward-regexp)
    ;; (define-key mbug-mode-map "B" 'bury-buffer)
    (define-key mbug-mode-map "d" 'mbug-delete)
    (define-key mbug-mode-map "q" 'mbug-logout)
    (define-key mbug-mode-map "K" 'mbug-kill-folder)
    (define-key mbug-mode-map "m" 'mbug-move)
    (define-key mbug-mode-map "S" 'mbug-show-structure)
    (define-key mbug-mode-map "u" 'mbug-undelete)
    (define-key mbug-mode-map "x" 'mbug-expunge)
    (define-key mbug-mode-map "X" 'mbug-spam)
    (define-key mbug-mode-map "n" 'mbug-new-mail)
    )
  (use-local-map mbug-mode-map)
  ;;set the mode as a non-editor mode
  (put 'mbug-mode 'mode-class 'special)
  ;;specify the mode name
  (setq mode-name "mbug")
  (setq major-mode 'mbug-mode)
  ;;setup the buffer to be modal
  (setq buffer-read-only 't)
  ;;specify that this buffer has been initialized with the major mode
  (make-local-variable 'mbug-mode-initialized-p)
  (setq mbug-mode-initialized-p 't)
  ;;ensure that paragraphs are considered to be whole mailing lists
  (make-local-variable 'paragraph-start)
  (setq paragraph-start "^[A-Za-z0-9]")
  ;; Ensure the undo doesn't get recorded for this buffer
  (buffer-disable-undo)
  ;;setup the kill-buffer stuff
  (make-local-variable 'kill-buffer-hook)
  (add-hook 'kill-buffer-hook 'mbug-kill-buffer-hook)
  ;; Make the connection local
  (make-local-variable 'mbug-connection)
  ;;make the username and password local
  ;; (make-local-variable 'mbug-username)
  ;; (make-local-variable 'mbug-password)
  ;;run the mode hooks
  (run-hooks 'mbug-mode-hook))

(defvar mbug-message-mode-map
  (let ((map (make-sparse-keymap)))
    map)
  "Keymap for `mbug-message-mode'.")

(define-derived-mode mbug-message-mode message-mode "Mbug Message"

  "The \"Message\" mode: View and interact with a message.
Available commands in the message composition buffer:

\\{mbug-message-mode-map}"

  (unless mbug-message-keymap-initializedp
    (define-key mbug-message-mode-map "\r" 'mbug-message-open-attachment)
    (define-key mbug-message-mode-map "a" 'message-wide-reply)
    (define-key mbug-message-mode-map "h" 'mbug-toggle-headers)
    (define-key mbug-message-mode-map "s-i" 'message-insert-or-toggle-importance)
    (define-key mbug-message-mode-map "q" 'mbug-kill-buffer)
    (define-key mbug-message-mode-map "r" 'message-reply)
    (setq mbug-message-keymap-initializedp 't))
  
  ;;set the mode as a non-editor mode
  (put 'mbug-message-mode 'mode-class 'special)
  ;;ensure that paragraphs are considered to be whole mailing lists
  (make-local-variable 'paragraph-start)
  (setq paragraph-start paragraph-separate)
  ;;setup the buffer to be read only
  ;; (make-local-variable 'buffer-read-only)
  (setq buffer-read-only 't)
  ;;run the mode hooks
  (run-hooks 'mbug-message-mode-hook))

(add-hook
 'message-mode-hook
 (lambda ()
   (define-key message-mode-map (kbd "C-<return>")
     (lambda ()
       (interactive)
       (mbug-send-mail)))))

(defun mbug-kill-buffer ()
  "Kill the buffer."
  (interactive)
  (kill-buffer (current-buffer))
  (delete-window))

(defun mbug-new-mail ()
  "Compose a new mail."
  (interactive)
  (message-mail))

(defun mbug-show-structure (folder-name uid)
  "Read info hidden in the text by means of `add-text-properties'.
Here, various info about the structure of the message in
FOLDER-NAME that has UID."
  (interactive (list (get-text-property (point) 'FOLDER)
                     (get-text-property (point) 'UID)))
  (imap-mailbox-select folder-name nil mbug-connection)
  (imap-fetch uid "(BODYSTRUCTURE)" 't nil mbug-connection)
  (print (imap-message-get uid 'BODYSTRUCTURE mbug-connection)))

(defun mbug-open ()
  "Expand/contract the folder or open the message that point is on."
  (interactive)
  (mbug-ensure-connected)
  (beginning-of-line)
  ;; (if (looking-at "^[^ \t\n\r]+")
  (if (get-text-property (point) 'help-echo)
      ;; Must be a folder... expand or contract according to current state.
      (let ((folder-path (get-text-property (point) 'help-echo)))
        (if (imap-mailbox-get 'OPENED folder-path mbug-connection)
            ;; Mark the mailbox
            (imap-mailbox-put 'OPENED nil folder-path mbug-connection)
          ;; Mark the folder opened
          (imap-mailbox-put 'OPENED 't folder-path mbug-connection))
        (mbug-redraw))
    ;; Must be a message, mark it seen and then open it.
    (let ((msg nil)
          (folder-path (get-text-property (point) 'FOLDER))
          (uid (get-text-property (point) 'UID)))
      (setq msg (cons uid (mbug-date-format
                           (imap-message-envelope-date uid mbug-connection))))
      (imap-message-flags-add (number-to-string uid) "\\Seen" nil mbug-connection)

      (if (and mbug-modal (eq window-system 'x))
          (progn
            (when (= (length (window-list)) 1)
              (when mbug-dedicated
                (set-window-dedicated-p (selected-window) (not current-prefix-arg)))
              (split-window-vertically 15)
              (other-window 1)
              (set-buffer-modified-p nil))))
      (mbug-message-open uid folder-path))))

(defun lookup (key list) ; This function is used via dynamic scope in some funcs called from here
  "Find the value following the KEY in the LIST, eg:
\(lookup 'nic '(bob 12 fred 73 mike 18 nic 34 jim 22))
 => 34"
  (when (member key list)
    (cadr (member key list))))

(defun extract-fist (list)
  "Return the first atom of LIST that is a list."
  ;; (message "LIST: %S" list)
  ;; list
  ;; (message "charset: %S" (assoc-recursive list 'charset))
  (cl-position "charset" list)
  
  )

(defun assoc-recursive (alist &rest keys)
  "Recursively find KEYs in ALIST."
  (while keys
    (setq alist (cdr (assoc (pop keys) alist))))
  alist)

(defun part-spec-p (str)
  "Is str a valid IMAP part specifier?"
  (and (stringp str) (string-match "[0-9][0-9.]*" str)))

(defun extract-list (atom)
  "Recursively find the first atom of ATOM that is a list."
  (if (and
       (listp atom)
       (stringp (car atom)))
      atom
    (extract-list (car atom))))

(defun mbug-bs-to-part-list-new (bs)
  "Make a part list from a bodystructure.
A part list is a flat list of all mime types, which are
alists. The part id is made an entry in the mime type with the
key: 'partnum"
  (let ((value)
        (big-list '()))
    (dolist (element bs value)
      (when (and
             (stringp (car element))
             (string-match "[0-9][0-9]*" (car element)))
        (add-to-list 'big-list (push
                                (cons 'partnum (car element))
                                (cdr element)) t)))
    big-list))

;; FIXME: This iterator stuff has got to go away.
(defun mbug-bs-to-part-list (bs)
  "Make a part list from a bodystructure.
A part list is a flat list of all mime types, which are
alists. The part id is made an entry in the mime type with the
key: 'partnum"
  (message "bs: %S" bs)
  
  (let ((parts (list '())))
    (defun iterator (lst)
      (mapc (lambda (item)
	      (and (listp item)
		   (part-spec-p (car item))
		   (nconc parts (list (cons (cons 'partnum (car item))
					    (cdr item))))
		   (iterator item)))  lst))
    (iterator bs)
    (cdr parts))) 

(defun mbug-bs-to-parts (atom)
  "Make a part list from a bodystructure.
A part list is a flat list of all mime types, which are
alists. The part id is made an entry in the mime type with the
key: 'partnum"
  (let ((big-list '()))
    (mapc (lambda (x)
            (message "x: %S" x)
            (when (and
                   (stringp (car x))
                   (string-match "[0-9][0-9]*" (car x)))
              (add-to-list 'big-list (push
                                      (cons 'partnum (car x))
                                      (cdr x)) t)))
          atom)
    big-list))

(defun mbug-message-open (uid folder-path)
  "Open UID in FOLDER-PATH."
  (interactive "Mfolder-path:\nnUid:")

  ;; Main func.
  (imap-mailbox-select folder-path nil mbug-connection)
  (imap-fetch uid "(BODYSTRUCTURE ENVELOPE RFC822.HEADER)" 't nil mbug-connection)
  (let* ((message-buffer-name (concat "message-" folder-path "-" (number-to-string uid)))
         (buf (if (get-buffer message-buffer-name)
                  (switch-to-buffer message-buffer-name)
                (get-buffer-create message-buffer-name)))
         (bs-def (imap-message-get uid 'BODYSTRUCTURE mbug-connection))
         (bs (mbug-parse-bs bs-def))
         ;; (parts (mbug-bs-to-part-list bs))
         (parts (mbug-bs-to-parts bs))

         (text-part (if parts
                        (progn
                          (message "Parts!")
                          (mbug-part-list-assoc 'type '(("text" . "plain")) parts))
                      bs))
         (text-chars (cadr (third (extract-list bs-def))))
         (text-trans (nth 6 (extract-list bs-def)))

         (message-header-format-alist
          `((From)
            (To)
            (Bcc)
            (Date)
            (Subject))))

    ;; (message "parts: %S" parts)
    ;; (message "partz: %S" partz)
    ;; (message "MESSAGE BODY: %S" (imap-message-get uid 'BODY))
    ;; First insert the header.
    (let ((hdr (imap-message-get uid 'RFC822.HEADER mbug-connection)))
      (with-current-buffer buf

        (insert (rfc2047-decode-string hdr))

        ;; Do SMTP transport decoding on the message header.
        (subst-char-in-region (point-min) (point-max) ?\r ?\ )
        (message-sort-headers)
        (make-local-variable 'mbug-message-text-end-of-headers)
        (setq mbug-message-text-end-of-headers (point))

        (put 'mbug-message-text-end-of-headers 'permanent-local 't)

        (insert (concat (mbug-char-repeat "-" (- (third (window-edges)) 10))) "\n\n")))
    
    (save-excursion
      ;; Now insert the first text part we have
      (when text-part
        (setq mbug--mail-body (mbug-message-get-body uid (if
                                                             text-part
                                                             (progn
                                                               (message "PART! [%S]" text-part)
                                                               text-part)
                                                           (message "BS! [%S]" bs)
                                                           bs) text-chars text-trans)))
      
      ;; Now switch to buffer wether we're in modal mode or not
      (switch-to-buffer buf)

      (insert mbug--mail-body)
      
      (if mbug-short-headers
          (progn
            (setq hide-region-overlays ())
            (mbug-toggle-headers)))
      (set-buffer-modified-p nil)
      (mbug-message-mode))

    ;; Display the list of other parts (if there are any) here
    (mbug-part-list-display mbug-connection folder-path uid buf parts)
    ;; (mbug-render-html buf)
    ))

(defun mbug-message-get-body (uid text-part charset transfer-encoding)
  "Return the text-part for the specified uid in current buffer.
UID is the id, TEXT-PART, BUFFER, CHARSET and TRANSFER-ENCODING
are used to construct the message."
  (message "fetch: %S" 
           (imap-fetch uid
                       (format "(BODY[%s])" (or (cdr (assoc 'partnum text-part)) "1"))
                       't nil mbug-connection))
  (let* ((start-of-body 0)
         (body (elt (car (imap-message-get uid 'BODYDETAIL mbug-connection)) 2)))
    
    ;; (message "body: %S" (imap-message-get uid 'BODYDETAIL mbug-connection))
    (mbug-decode-string
     body
     transfer-encoding
     (cond
      ((and (equal charset "us-ascii")
            (equal transfer-encoding "8bit")) 'utf-8)
      (charset charset)
      ('t 'emacs-mule)))))

(defun mbug-message-fill-text (uid text-part buffer charset transfer-encoding)
  "Insert the text-part for the specified uid in the buffer provided.
UID is the id, TEXT-PART, BUFFER, CHARSET and TRANSFER-ENCODING
are used to construct the message."
  (imap-fetch uid
              (format "(BODY[%s])" (or (cdr (assoc 'partnum text-part)) "1"))
              't nil mbug-connection)
  (let* ((start-of-body 0)
         (body (elt (car (imap-message-get uid 'BODYDETAIL mbug-connection)) 2)))
    
    (save-excursion
      (switch-to-buffer buffer)
      (setq start-of-body (point))

      (insert (mbug-decode-string
               body
               transfer-encoding
               (cond
                ((and (equal charset "us-ascii")
                      (equal transfer-encoding "8bit")) 'utf-8)
                (charset charset)
                ('t 'emacs-mule)))))))

(defun mbug-render-html (buf)
  "Render the buffer HTML."
  (let ((start)
        (end)
        (inhibit-read-only t)
        (buffer-read-only nil))
    (with-current-buffer buf
      (when
          (re-search-forward myregexp-html-start nil t nil)
        (beginning-of-line)
        (setq start (point)))
      (when
          (re-search-forward myregexp-html-end nil t nil)
        (end-of-line)
        (setq end (point)))
      (when (and start end)
        (shr-render-region start end)
        (restore-buffer-modified-p nil)))))

(defun mbug-part-list-display (connection folder uid buffer part-list)
  "Given CONNECTION, open FOLDER for UID in BUFFER, and display PART-LIST."
  (with-current-buffer buffer
    (make-local-variable 'mbug-connection)
    (setq mbug-connection connection)
    ;; (message "#### part-list: %S" part-list)
    (let ((buffer-read-only nil))
      (save-excursion
        (goto-char (point-max))
        (insert "\n--attachment links follows this line--\n")
        (mapc (lambda (part)
                (let ((partnum (cdr (assoc 'partnum part)))
                      (name (lookup "name" (cadr (assoc 'body part)))))
                  (if (> (- (point) (line-beginning-position)) 72)
                      (insert "\n"))
                  (let ((pt (point)))
                    (if name
                        (insert (format "\nAttached: %s" name)))
                    (add-text-properties pt (point) `(PARTNUM ,partnum FOLDER ,folder UID ,uid)))))
              part-list)
        (set-buffer-modified-p nil)))))


;; Sub-part handling
(defun mbug-message-open-attachment ()
  "Open message attachment."
  ;; FIXME:: This could be merged into mbug-message-open-part
  ;;
  ;; mbug-message-open-part needs to have an interactive that looks
  ;; something like this:
  ;;
  ;; (if (looking-at "Attached:.*\\[[0-9.]\\]")
  ;;     (list (get-text-property (point) 'FOLDER)
  ;;            ...)
  ;;   (read-from-minibuffer "..." ....))
  (interactive)
  (let ((folder-name (get-text-property (point) 'FOLDER))
        (uid (get-text-property (point) 'UID))
        (partnum (get-text-property (point) 'PARTNUM)))
    ;; If there is no UID then it's probably not an attachment
    (if uid
        (mbug-message-open-part folder-name uid partnum mbug-connection)
      (goto-address-at-point))))

(defun mbug-buttonize-attachment-links (type)
  "Turn all found strings into buttons of type TYPE."
  (save-excursion
    (goto-char (point-min))
    (let ((regexp (cond ((eq type 'dsp) faustine-regexp-faust-file)
                        ((eq type 'log) faustine-regexp-log)
                        ((eq type 'exe) faustine-regexp-exe)
                        ((eq type 'lib) faustine-regexp-lib))))
      (while (re-search-forward regexp nil t nil)
        (if
            (not (eq 'comment (syntax-ppss-context (syntax-ppss))))
            (progn
              (remove-overlays (match-beginning 1) (match-end 1) nil nil)
              (make-button (match-beginning 1) (match-end 1)
                           :type (intern-soft (concat "faustine-button-" (symbol-name type)))))
          (remove-overlays (match-beginning 1) (match-end 1) nil nil))))))

;; We need to modularize this so we can have a dump function as well
;; I think we should pass the function to handle the part in and have this call it
(defun mbug-message-open-part (folder-name uid partnum &optional imap-con)
  "Open the specified part.
FOLDER-NAME, UID and PARTNUM form the part.
In MBUG the imap connection is obtained through the buffer.
Programs can pass IMAP-CON in directly though."
  (interactive "MFolder-name:\nnUid:\nMPart:")
  (or imap-con (setq imap-con mbug-connection)) ; allows programs to pass in their own imap-con
  (imap-mailbox-select folder-name nil imap-con)
  (imap-fetch uid (format "(BODY[%s])" partnum) 't nil imap-con)
  (imap-fetch uid "(BODYSTRUCTURE)" 't nil imap-con)
  (let ((multipart (mbug-parse-bs (imap-message-get uid 'BODYSTRUCTURE imap-con))))
    (let* ((msg-buffer (current-buffer)) ; only needed so we can associate attachment processes with it
           (part-list (mbug-bs-to-part-list multipart))
           (part (mbug-part-list-assoc 'partnum partnum part-list))
           (mimetype (cadr (assoc 'type part)))

           ;; pX: We need this to name the attachment buffer (needed for both inline and mailcap)
           (name (lookup "name" (cadr (assoc 'body part))))

           (start-of-body 0)
           (mimetype-str (concat (car mimetype) "/" (cdr mimetype)))
           (mimetype (car mimetype))
           (buffer (get-buffer-create "*mbug-attachment*"))
           ;; (buffer (get-buffer-create (concat "*attached-" name "*")))
           )

      ;; pX: This was a swith-to-buffer
      (set-buffer buffer)

      (setq start-of-body (point))

      ;; Do a mailcap view if we have a viewer, otherwise ask for a program
      (mailcap-parse-mailcaps)
      (let ((mailcap-viewer (if (mailcap-mime-info mimetype-str)
                                (mailcap-mime-info mimetype-str)
                              (concat (read-from-minibuffer
                                       (format "No %s in ~/.mailcap, open `%s' with: " mimetype-str name))
                                      " %s")))

            (mailcap-ext-pattern (mailcap-mime-info mimetype-str "nametemplate"))
            (mailcap-ext-pattern-all (mailcap-mime-info mimetype-str "all")))

        ;; Display in the viewer.
        (if mailcap-viewer
            (progn
              (mbug-attachment-handle msg-buffer mimetype)
              (setq buffer-read-only 't)
              (set-buffer-modified-p nil))

          
          ;; else we don't have a mailcap viewer
          ;;  --- FIXME: - sure this could be integrated with viewer stuff above
          ;;  --- ask for a viewer?
          (progn
            (insert (mbug-decode-string
                     ;; This gets the body and can be expensive
                     (elt (car (imap-message-get uid 'BODYDETAIL imap-con)) 2)
                     (cadr (assoc 'transfer-encoding part))
                     (lookup "charset" (cadr (assoc 'body part))))))
          ;; (normal-mode)
          (goto-char (point-min)))))))

(defun mbug-attachment-handle (calling-buffer mimetype)
  "Handle an attachment with some inline Emacs viewer.
Take both the CALLING-BUFFER and the MIMETYPE."
  ;; Extract the part and shove it in a buffer
  (let ((charset (or (lookup "charset" (cadr (assoc 'body part)))
                     (progn (set-buffer-multibyte nil)
                            'no-conversion)))

        (name (lookup "name" (cadr (assoc 'body part))))
        (enc (cadr (assoc 'transfer-encoding part)))
        (fname (if mailcap-ext-pattern
                   (make-temp-file "mbug-" nil (file-name-extension mailcap-ext-pattern t))
                 (make-temp-file (concat "mbug-" name) nil (file-name-extension name t)))))

    ;; Setup the buffer
    (insert (mbug-decode-string
             ;; This gets the body and can be expensive
             (elt (car (imap-message-get uid 'BODYDETAIL imap-con)) 2)
             enc charset))

    (write-region (point-min) (point-max) fname)

    ;; Now decide what sort of viewer came out of mailcap - unix process or elisp function?
    (if (functionp mailcap-viewer)
        ;; An emacs function... if it's a mode function then we just run it on the current buffer
        (progn
          ;; (message "-- mailcap-viewer: %s " mailcap-viewer)
          (if (string-match "[a-zA-Z0-9-]+-mode$" (symbol-name mailcap-viewer))
              (progn
                ;; (message "-- match")
                (with-current-buffer buffer
                  (funcall mailcap-viewer)))
            ;; Else we run it passing it the buffer
            (progn
              ;; (message "-- no match")
              (funcall mailcap-viewer buffer))))

      (if (and mbug-inline-images
               (string= "IMAGE" mimetype))
          (progn
            (switch-to-buffer calling-buffer)
            (setq inhibit-read-only 't)
            (insert "\n")
            (insert-image (create-image fname)))
        (let* ((proc-buf (generate-new-buffer "*mbug-attachment*"))
               (proc (apply 'start-process-shell-command
                            `("*mbug-detachment*" ,proc-buf
                              ,@(split-string (format mailcap-viewer fname)) )) ))
          (set-process-sentinel proc 'mbug-attachment-sentinel))))))

(defun mbug-attachment-sentinel (process event)
  "Monitor PROCESS for EVENT."
  (let ((buf (process-buffer process))
        (state (process-status process)))
    (when (and (not (eq state 'run))
               (not (eq state 'stop))
               (< (buffer-size buf) 1))
      (kill-buffer "*mbug-attachment*"))))

(defun mbug-decode-string (content transfer-encoding char-encoding)
  "Decode the specified CONTENT string according to TRANSFER-ENCODING and CHAR-ENCODING."
  (let* ((transfer-enc (if transfer-encoding
                           (upcase transfer-encoding)
                         'NONE))
         (char-enc (let ((encoding
                          (if char-encoding
                              (intern (downcase
                                       (if (stringp char-encoding)
                                           char-encoding
                                         (symbol-name char-encoding))))
                            char-encoding)))
                     (if (and encoding (memq encoding coding-system-list))
                         encoding
                       'no-conversion))))
    ;; (message "content decode: %s transfer-encoding: %s char-encoding: %s" content transfer-encoding char-encoding)
    (cond
     ((equal transfer-enc "QUOTED-PRINTABLE")
      (decode-coding-string
       (quoted-printable-decode-string
        (replace-regexp-in-string "\r" "" content))
       char-enc))
     ((equal transfer-enc "BASE64")
      (decode-coding-string (base64-decode-string content) char-enc))
     ('t (if (string= "" char-enc)
             (rfc2047-decode-string content)
           (decode-coding-string content char-enc))))))

(defvar mbug-message-date-regex
  "[0-9]\\{1,4\\}-[0-9]\\{2\\}-[0-9]\\{2\\} "
  "Regex for matching an mbug message date.")

(defvar mbug-message-time-regex
  "\\(\\([0-9][\t ]\\)\\|\\([0-9]\\{2\\}:[0-9]\\{2\\}:[0-9]\\{2\\}\\)\\)"
  "Regex for matching an mbug message time.")

(defvar mbug-message-line-regex
  (concat "^[\t ]+"
          mbug-message-date-regex
          mbug-message-time-regex
          ;; email address match
          "[\t ]+\\([^\t\n ]+\\)"
          ;; subject match
          "[\t ]+\\([^\t\n]+\\)$")
  "Regex for matching an mbug message.
Broadly this is: DATE TIME FROM SUBJECT.")

(defun mbug-send-mail ()
  "Send a mail, and kill the buffer."
  (interactive)
  (message-send)
  (mbug-kill-buffer))

(defun mbug-mark-regex (regex)
  "Mark a message matched by REGEX for some other operation."
  (interactive "Mregex to match message lines: ")
  (save-excursion
    (goto-char (mbug-beginning-of-folder (get-text-property (point) 'FOLDER)))
    (while (re-search-forward regex nil 't)
      (progn
        (let ((inhibit-read-only 't))
          (add-text-properties
           (point-at-bol)
           (point-at-eol)
           `(marked t
                    face ,mbug-px-face-marked)))))))

(defun mbug-beginning-of-folder (folder)
  "Find FOLDER and move point to the start of it."
  (goto-char 0)
  (re-search-forward (concat "^" folder " $")))

(defun mbug-delete-marked ()
  "Delete messages that have been marked in the current folder."
  (interactive)
  (save-excursion
    (let ((folder-name (get-text-property (point) 'FOLDER)))
      (mbug-beginning-of-folder folder-name)
      (imap-mailbox-select folder-name nil mbug-connection)
      (while (re-search-forward mbug-message-line-regex nil 't)
        (progn
          (if (get-text-property (point-at-bol) 'marked)
              (let ((uid (get-text-property (point-at-bol) 'UID)))
                (imap-fetch uid "(ENVELOPE)" 't nil mbug-connection)
                ;; this should be on a switch
                (imap-message-copy
                 (number-to-string uid) mbug-trash-folder-name 't 't mbug-connection)
                (imap-message-flags-add
                 (number-to-string uid) "\\Deleted" nil mbug-connection)
                (let ((msg (cons uid (mbug-date-format
                                      (imap-message-envelope-date uid mbug-connection)))))
                  (mbug-msg-redraw (current-buffer) folder-name msg)))))))))

(defun mbug-delete-message (folder-name uid)
  "Delete a message.
When called interactively FOLDER-NAME and UID are obtained from
the text properties of whatever is at (point)."
  (interactive (list (get-text-property (point) 'FOLDER)
                     (get-text-property (point) 'UID)))
  (beginning-of-line)
  (mbug-ensure-connected)
  (imap-mailbox-select folder-name nil mbug-connection)
  (imap-fetch uid "(ENVELOPE)" 't nil mbug-connection)
  (imap-message-copy (number-to-string uid) mbug-trash-folder-name 't 't mbug-connection)
  ;; this should be on a switch
  (imap-message-flags-add (number-to-string uid) "\\Deleted" nil mbug-connection)
  (let ((msg (cons uid
                   (mbug-date-format
                    (imap-message-envelope-date uid mbug-connection)))))
    (mbug-msg-redraw (current-buffer) folder-name msg)))

(defun mbug-undelete-message (folder-name uid)
  "Undelete a message."
  (interactive (list (get-text-property (point) 'FOLDER)
                     (get-text-property (point) 'UID)))
  (mbug-ensure-connected)
  (imap-mailbox-select folder-name nil mbug-connection)
  (imap-fetch uid "(ENVELOPE)" 't nil mbug-connection)
  (imap-message-flags-del (number-to-string uid) "\\Deleted" nil mbug-connection)
  (let ((msg (cons uid
                   (mbug-date-format
                    (imap-message-envelope-date uid mbug-connection)))))
    (mbug-msg-redraw (current-buffer) folder-name msg)))

(defun mbug-delete ()
  "Mark one more message for deletion ; Press `x' to execute."
  (interactive)
  (if (and transient-mark-mode mark-active)
      (mbug-mark-region 'mbug-delete-message)
    (call-interactively 'mbug-delete-message)))

(defun mbug-undelete ()
  "Unmark one more condemned message."
  (interactive)
  (if (and transient-mark-mode mark-active)
      (mbug-mark-region 'mbug-undelete-message)
    (call-interactively 'mbug-undelete-message)))

(defun mbug-mark-region (function)
  "Mark several messages and execute FUNCTION upon them."
  (save-excursion
    (let ((lines))
      (setq lines (count-lines (region-beginning) (region-end)))
      ;; (message "Mbug - Marking %s messages" lines)
      (goto-char (region-beginning))
      (dotimes (number lines)
        (call-interactively function))
      (message "Done."))))

(defun mbug-spam (folder-name uid)
  "Move the message from FOLDER-NAME to a spam folder.
The Spam folder name is `mbug-spam-folder-name' which can be
customized, the UID is the messsage."
  (interactive (list (get-text-property (point) 'FOLDER)
                     (get-text-property (point) 'UID)))
  (mbug-move folder-name uid mbug-spam-folder-name))

(defun mbug-move (folder-name uid to-folder)
  "From FOLDER-NAME, move UID to TO-FOLDER."
  (interactive
   (let ((dest-folder
          (completing-read
           "Folder name: "
           (mapcar
            (lambda (folder-cell)
              (cons (cdr folder-cell) 't)) mbug-smart-folder-list)
           nil nil mbug-initial-folder-name 'mbug-folder-history)))
     (list (get-text-property (point) 'FOLDER)
           (get-text-property (point) 'UID)
           dest-folder)))
  (mbug-ensure-connected)
  (imap-mailbox-select folder-name nil mbug-connection)
  (imap-fetch uid "(ENVELOPE)" 't nil mbug-connection)
  (imap-message-copy (number-to-string uid) to-folder 't 't mbug-connection)
  (mbug-delete-message folder-name uid))

(defun mbug-expunge (folder-name doit)
  "Expunge the current FOLDER-NAME, given DOIT."
  (interactive (list (get-text-property (point) 'FOLDER)
                     (y-or-n-p "Expunge current folder?")))
  (mbug-ensure-connected)
  (if folder-name
      (imap-mailbox-select folder-name nil mbug-connection))
  (imap-mailbox-expunge 't mbug-connection)
  (imap-mailbox-unselect mbug-connection)
  (mbug-redraw))

(defun mbug-extract-folder-name ()
  "Extract the folder-name from the current line."
  (save-excursion
    (let ((folder-name (get-text-property (point) 'help-echo)))
      folder-name)))

(defun mbug-create-folder (new-folder-name)
  "Create a NEW-FOLDER-NAME under the specified parent folder."
  (interactive (list (completing-read
                      "New folder name: "
                      (mapcar
                       (lambda (folder-name)
                         (cons folder-name 't)) mbug-folder-list))))
  (imap-mailbox-create new-folder-name mbug-connection)
  (imap-mailbox-list "*" "" "." mbug-connection)
  (mbug-redraw))

(defun mbug-kill-folder (folder)
  "Delete FOLDER under point."
  (interactive (let* ((dir (mbug-extract-folder-name))
                      (confirm (y-or-n-p (concat "Delete folder " dir))))
                 (if confirm (list dir))))
  (if folder
      (progn
        (imap-mailbox-delete folder mbug-connection)
        (imap-mailbox-list "*" "" "." mbug-connection)
        (mbug-redraw))))

(defun mbug-logout ()
  "Close the IMAP mail server connection.
Just do anything to open it back again."
  (interactive)
  (setq global-mode-string ())
  (if mbug-connection
      (progn
        (message "Connection closed")
        (imap-close mbug-connection)))
  (setq mbug-connection nil))

(defun mbug-redraw ()
  "Redraw/refresh the message list buffer based on the imap state.
Opened folders have their messages re-read and re-drawn."
  (interactive)

  (if init
      (progn
        (if mbug-bug
            (mbug-timer-start))
        (mbug-menu)
        (setq init ())))

  ;; (setq stored-pos (point))
  (defun insert-with-prop (text prop-list)
    (let ((pt (point)))
      (insert text)
      (add-text-properties pt (point) prop-list)))

  ;; Main function.
  (mbug-ensure-connected)
  (let (
        (stored-pos (point))
        (inhibit-read-only 't)
        (display-buffer (current-buffer)))
    (delete-region (point-min) (point-max))
    (mbug-refresh-folder-list)

    ;; Map the folder display over the sorted smart folder list - new mapc.
    (mapc
     (lambda (folder-cell)
       (with-current-buffer display-buffer

         (let ((folder-name (car folder-cell))
               (folder-path (cdr folder-cell))
               (folder-depth (mbug-count-occurrences "/" (cdr folder-cell))))
           (if (imap-mailbox-get 'OPENED folder-path mbug-connection)
               (setq folder-icon "▾")
             (setq folder-icon "▸"))

           (insert (propertize (concat
                                (mbug-char-repeat " " folder-depth)
                                folder-icon " " folder-name) 'face 'mbug-face-folder))
           (put-text-property
            (line-beginning-position) (+ 1 (line-beginning-position)) 'help-echo folder-path)

           (insert " \n")
           (if (imap-mailbox-get 'OPENED (cdr folder-cell) mbug-connection)
               (let* ((selection
                       ;; Force the re-selection of the folder before local vars
                       (progn
                         (imap-mailbox-unselect mbug-connection)
                         (imap-mailbox-select (cdr folder-cell) nil mbug-connection)))
                      (existing (imap-mailbox-get 'exists (cdr folder-cell) mbug-connection))
                      (message-range (concat "1:" (number-to-string existing))))
                 (imap-fetch message-range "(UID FLAGS ENVELOPE)" nil 't mbug-connection)

                 (setq openingp 't)
                 ;; Map the message redraw over each message in the folder.
                 (mapc
                  (lambda (msg)
                    (let ((msg-redraw-func (mbug-get-msg-redraw-func (cdr folder-cell))))
                      (funcall msg-redraw-func display-buffer (cdr folder-cell) msg)))

                  ;; The message list is sorted before being output
                  (sort
                   (imap-message-map
                    (lambda (uid property)
                      (cons uid
                            (condition-case nil
                                (timezone-make-date-sortable
                                 (mbug-date-format (elt property 0)) "GMT" "GMT")

                              ;; Ensures that strange dates don't cause a problem.
                              (range-error nil))))
                    'ENVELOPE mbug-connection)

                   ;; Compare the sort elements by date
                   (lambda (left right)
                     (string< (cdr left) (cdr right)))))

                 (insert "\n"))))))
     mbug-smart-folder-list)
    (goto-char stored-pos)
    (when openingp
      (search-forward-regexp "^$")
      (forward-line -1))
    (setq openingp 'nil)))

(defun mbug-get-msg-redraw-func (folder-name)
  "The function to redraw FOLDER-NAME."
  'mbug-msg-redraw)

(defun mbug-msg-redraw (display-buffer folder-name msg)
  "Redraw a single message line.
In DISPLAY-BUFFER, redraw FOLDER-NAME.
MSG is a dotted pair such that: ( uid . msg-date )"
  ;; The passing of the (uid . msg-date) could be improved...
  ;; it's done like that so mbug-redraw can sort and map the
  ;; messages all in one... but it means multiple calls to
  ;; mbug-date-format which is perhaps slow.

  (with-current-buffer display-buffer
    (let* ((inhibit-read-only 't)
           (uid (car msg))
           (date (mbug-date-format (imap-message-envelope-date uid mbug-connection)))
           ;; (date (cdr msg))
           (from-addr
            (mbug-from-format
             (let ((env-from (imap-message-envelope-from uid mbug-connection)))
               (if (consp env-from)
                   (car env-from)
                 ;; Make up an address
                 `("-" "unknown email" "-" "-")))))
           (to-addr
            (mbug-from-format
             (let ((env-to (imap-message-envelope-to uid mbug-connection)))
               (if (consp env-to)
                   (car env-to)
                 ;; Make up an address
                 `("-" "unknown email" "-" "-")))))
           (subject
            (mbug-field-format (imap-message-envelope-subject uid mbug-connection) 1 't))
           (line-start (point))

           (message-face
            (cond
             ((mbug-flag-p uid "\\Deleted") 'font-lock-warning-face)
             ((mbug-flag-p uid "\\Answered") 'font-lock-keyword-face)
             ((not (mbug-flag-p uid "\\Seen")) 'mbug-face-folder)
             ((string-match "Sent" folder-name) 'font-lock-builtin-face)
             (t 'default))))

      (beginning-of-line)
      (if (> (- (line-end-position) (point)) 0)
          (progn
            ;; Ensure the current line is deleted
            (delete-region (line-beginning-position) (line-end-position))
            (delete-char 1)))

      (insert
       " " (mbug-field-format date 20)
       " "
       (if (string-match "Sent" folder-name)
           (mbug-field-format to-addr 25)
         (mbug-field-format from-addr 25))
       " " (rfc2047-decode-string subject) "\n")
      (add-text-properties line-start (point)
                           `(UID ,uid FOLDER ,folder-name face ,message-face)))))

(defun mbug-recount ()
  "Recount 'INBOX' based on the imap state."
  (interactive)

  ;; (message "mbug-recount IN, mbug-connection: %s" mbug-connection)
  (setq mbug-unread-mails ())
  ;; (mbug-ensure-connected)
  (let ((display-buffer "mail-bug"))
    (with-current-buffer display-buffer
      (let* ((mbug-this-box "INBOX")
             (selection

              ;; Force the re-selection of the folder before local vars
              (progn
                (imap-mailbox-unselect mbug-connection)
                (imap-mailbox-select mbug-this-box nil mbug-connection)))
             (existing (imap-mailbox-get 'exists mbug-this-box mbug-connection))
             (message-range (concat "1:" (number-to-string existing))))
        (imap-fetch message-range "(UID FLAGS ENVELOPE)" nil 't mbug-connection)

        ;; Map the message recount over each message in the folder.
        (mapc
         (lambda (msg)
           (let ((msg-recount-func (mbug-get-msg-recount-func mbug-this-box)))
             (funcall msg-recount-func display-buffer mbug-this-box msg)))

         ;; The message list is sorted before being output
         (sort
          (imap-message-map
           (lambda (uid property)
             (cons uid
                   (condition-case nil
                       (timezone-make-date-sortable (mbug-date-format (elt property 0)) "GMT" "GMT")

                     ;; Ensures that strange dates don't cause a problem.
                     (range-error nil))))
           'ENVELOPE mbug-connection)

          ;; Compare the sort elements by date
          (lambda (left right)
            (string< (cdr left) (cdr right))))))))
  ;; Refresh the modeline
  (progn (setq global-mode-string ())
         (add-to-list 'global-mode-string
                      (mbug-mode-line mbug-unread-mails))
         ;; Bug the user
         (mbug-desktop-notify-smart)))

(defun mbug-get-msg-recount-func (folder-name)
  "The function to recount FOLDER-NAME."
  'mbug-msg-recount)

(defun mbug-msg-recount (display-buffer folder-name msg)
  "Recount a single message line.
In DISPLAY-BUFFER, recount FOLDER-NAME given MSG."
  (with-current-buffer display-buffer
    (let* ((uid (car msg))
           (date (mbug-date-format (imap-message-envelope-date uid mbug-connection)))
           (from-addr
            (mbug-from-format
             (let ((env-from (imap-message-envelope-from uid mbug-connection)))
               (if (consp env-from)
                   (car env-from)
                 `("-" "unknown email" "-" "-")))))
           (subject
            (imap-message-envelope-subject uid mbug-connection))

           (message-unread
            (cond
             ((not (mbug-flag-p uid "\\Seen"))
              (list date from-addr subject uid))
             (t nil))))

      (if message-unread
          (add-to-list 'mbug-unread-mails message-unread)))))


;; REPLY Auto-BCC + Yank
(defadvice message-reply (after mbug-message-reply-yank-original)
  "BCC to sender, quote original."
  (if mbug-bcc-to-sender
      (progn
        (message-replace-header "BCC" "xxx@xxx.com" "AFTER" "FORCE")))
  (message-yank-original)
  (message-sort-headers)
  (message-goto-body)
  (newline)
  (forward-line -1)
  (set-buffer-modified-p nil))
(ad-activate 'message-reply)

;; SEND Auto-BCC + Yank
(defadvice message-mail (after mbug-message-mail-bcc)
  "BCC to sender."
  (message-replace-header "BCC" user-mail-address "AFTER" "FORCE")
  ;; (message-replace-header "Reply-To" "Philippe Coatmeur-Marin <philcm@gnu.org>" "AFTER" "FORCE")
  (message-sort-headers)
  (set-buffer-modified-p nil)
  (message-goto-to))

(ad-activate 'message-mail)


;; Hide headers
(defun mbug-toggle-headers ()
  "Toggle headers."
  (interactive)
  (if (car hide-region-overlays)
      (progn
        (mbug-show-headers)
        (goto-char 0)
        (setq hide-region-overlays ()))
    (progn
      (goto-char 0)
      (search-forward-regexp "Subject")
      (end-of-line)
      (setq buffer-read-only nil)
      ;; (next-line)
      (forward-line)
      (beginning-of-line)
      (push-mark)
      (forward-paragraph)
      (mbug-hide-headers)
      (deactivate-mark))))


(defun mbug-show-headers ()
  "Unhide a region at a time, starting with the last one.
Hide and delete the overlay from the hide-region-overlays \"ring\"."
  (interactive)
  (when (car hide-region-overlays)
    (delete-overlay (car hide-region-overlays))
    (setq hide-region-overlays (cdr hide-region-overlays))))


(defun mbug-hide-headers ()
  "Hide a region by making an invisible overlay over it.
Save the overlay in the hide-region-overlays \"ring\""
  (interactive)
  (let ((start (point))
        (new-overlay (make-overlay (mark) (point))))
    (push new-overlay hide-region-overlays)
    (overlay-put new-overlay 'invisible t)
    ;; (overlay-put new-overlay 'intangible t)
    (overlay-put new-overlay 'before-string
                 (if hide-region-propertize-markers
                     (propertize hide-region-before-string
                                 'font-lock-face 'mail-bug-toggle-headers-face)
                   hide-region-before-string))
    (overlay-put new-overlay 'after-string
                 (if hide-region-propertize-markers
                     (propertize hide-region-after-string
                                 'font-lock-face 'mail-bug-toggle-headers-face)
                   hide-region-after-string))))

(defun mbug-mode-line (mbug-unseen-mails)
  "Construct an Emacs modeline object and display MBUG-UNSEEN-MAILS."
  (setq nice-uri (replace-regexp-in-string "^.*?\\." "" mbug-imap-host-name 't))
  (if (null mbug-unseen-mails)
      " "
    (let ((s (format "%d" (length mbug-unseen-mails)))
          (map (make-sparse-keymap))
          (url (concat "http://" nice-uri)))

      (define-key map (vector 'mode-line 'mouse-1)
        `(lambda (e)
           (interactive "e")
           (switch-to-buffer "mail-bug")))

      (define-key map (vector 'mode-line 'mouse-2)
        `(lambda (e)
           (interactive "e")
           (browse-url ,url)))

      (add-text-properties 0 (length s)
                           `(local-map
                             ,map mouse-face mode-line-highlight uri
                             ,url help-echo
                             ,(format "
%s
_x_____________________________________
mouse-1: View in mail-bug
mouse-2: View on %s" (mbug-tooltip) url))
                           s)
      (concat mail-bug-logo ":" s))))

(defun mbug-newmails (mail-list)
  "Count the new mails in MAIL-LIST."
  ;; (mbug-desktop-notification
  ;;  (concat "New mail" (if (> (length mail-list) 1) "s" ""))
  ;;  (rfc2047-decode-string (mapconcat
  ;;                          (lambda (xx)
  ;;                            (mapconcat
  ;;                             (lambda (xxx)
  ;;                               (format "%s" xxx))
  ;;                             (butlast xx) "\n"))
  ;;                          mail-list "\n\n"))
  ;;  5000 mbug-new-mail-icon)
  (setq mbug-to-be-advertised-mails ()))

(defun mbug-desktop-notify-smart ()
  "Loop through the unread mails and advertise them in bulk if there are >1."
  (mapc
   (lambda (x)
     (if (not (member x mbug-advertised-mails))
         (progn
           (add-to-list 'mbug-advertised-mails x)
           (setq newmail 't)
           (add-to-list 'mbug-to-be-advertised-mails x))))
   mbug-unread-mails)
  (if mbug-to-be-advertised-mails (mbug-newmails mbug-to-be-advertised-mails)))

(defun dbus-capable ()
  "Check if dbus is available."
  (unwind-protect
      (let (retval)
        (condition-case ex
            (setq retval (dbus-ping :session "org.freedesktop.Notifications"))
          ('error
           (message (format "Error: %s - No dbus notifications capabilities" ex))))
        retval)))

(defun mbug-desktop-notification (summary body timeout icon)
  "Call notification-daemon method METHOD with ARGS over dbus.
Format a message using SUMMARY, BODY, TIMEOUT and ICON."
  (if (dbus-capable)
      (dbus-call-method
       :session                        ; use the session (not system) bus
       "org.freedesktop.Notifications" ; service name
       "/org/freedesktop/Notifications"   ; path name
       "org.freedesktop.Notifications" "Notify" ; Method
       "emacs"
       0
       icon
       summary
       body
       '(:array)
       '(:array :signature "{sv}")
       ':int32 timeout)
    (message "New mail!"))
  (if mbug-new-mail-sound
      (play-sound-file mbug-new-mail-sound)))

(defun mbug-tooltip ()
  "Loop through the mail headers and build the hover tooltip."
  (rfc2047-decode-string
   (mapconcat
    (lambda (x)
      (let
          ((tooltip-string
            (format "%s\n%s \n-------\n%s"
                    (first x)
                    (second x)
                    (third x))))
        tooltip-string))
    mbug-unread-mails
    "\n\n")))

;; Boot strap stuff
(add-hook 'kill-emacs (lambda () (mbug-logout)))

;; forced, campaign-grade SMTP
(mbug-eval-smtp)

;; This actually works
;; (ert-deftest foo ()
;;   (assert nil))

(provide 'mail-bug)

;; (setq mbug-advertised-mails ())
;; (mbug-desktop-notify)
;; (setq mbug-unread-mails ())
;; (mbug-desktop-notification "plip" "plop" 5000 "/usr/share/yelp/icons/hicolor/16x16/status/yelp-page-video.png")


;; Local Variables:
;; byte-compile-warnings: (not free-vars)
;; End:
;;; mail-bug.el ends here
