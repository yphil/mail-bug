# Mail-Bug
 *A purely IMAP based email client for Emacs.*
___
[![License GPLv3](https://img.shields.io/badge/license-GPL_v3-green.svg)](http://www.gnu.org/licenses/gpl-3.0.html) [![Codeship Status for yassinphilip/mail-bug](https://app.codeship.com/projects/2c0e30d0-66d5-0135-e9a2-16754ac7c510/status?branch=master)](https://app.codeship.com/projects/240973) 


Mail Bug is pure IMAP online-only mail client. I use it to read a
quick mail on my main account. It is probably **not** your new main
email client.

For starters, it only supports/displays *one* email account at a
time. And, it's as fast as your internet connection, So that's
that, too.

It has been improved lately in the area of:

- Authentication & security
- Subtle ergonomics
- Customization
- Charset decoding
- Message management
    - Moving
    - Spam flagging
- Attachment handling
- HTML rendering
- Replying & sending
- SMTP handling

## Installation
- Put/clone it in the `load-path` and then either:
    - Use `customize-group RET mail-bug RET`
    - Or set it up manually:
```elisp
(require 'mail-bug)

(setq mbug-imap-host-name "imap.hostname")
(setq mbug-smtp-host-name "smtp.hostname")
(setq mbug-username "username")
```
- Create a `~/.authinfo.gpg` file with a line for your IMAP server:
`machine YOURIMAPSERVER login YOURUSERNAME password YOURPASSWORD`
    - If your "machine" (server) is Gmail, you must use an "app password", [generate
one](https://myaccount.google.com/apppasswords) at google's.
- The SMTP entry will be created when you send your first mail.

## Usage
Run `mail-bug` to connect ; The keys/commands available to
read, send and manage messages are detailed below.

## Major modes

Mail-Bug introduces 2 major modes, detailed below.

### mbug-mode
The "list" mode.

Every command act on the object (folder or message) under point,
or on lines selected in the normal fashion.

Available commands in the message/folder list:

#### mbug-mode Keys

Key  | Binding 
------------- | ------------- 
`RET` | [mbug-open](#markdown-header-mbug-open)
`+` | [mbug-create-folder](#markdown-header-mbug-create-folder)
`K` | [mbug-kill-folder](#markdown-header-mbug-kill-folder)
`S` | [mbug-show-structure](#markdown-header-mbug-show-structure)
`X` | [mbug-spam](#markdown-header-mbug-spam)
`d` | [mbug-delete](#markdown-header-mbug-delete)
`m` | [mbug-move](#markdown-header-mbug-move)
`n` | [mbug-new-mail](#markdown-header-mbug-new-mail)
`q` | [mbug-logout](#markdown-header-mbug-logout)
`u` | [mbug-undelete](#markdown-header-mbug-undelete)
`x` | [mbug-expunge](#markdown-header-mbug-expunge)

#### mbug-mode Commands

##### mbug-open

```elisp
(mbug-open)
```

Expand/contract the folder or open the message that point is on.

##### mbug-create-folder

```elisp
(mbug-create-folder NEW-FOLDER-NAME)
```

Create a NEW-FOLDER-NAME under the specified parent folder.

##### mbug-kill-folder

```elisp
(mbug-kill-folder FOLDER)
```

Delete FOLDER under point.

##### mbug-show-structure

```elisp
(mbug-show-structure FOLDER-NAME UID)
```

Read info hidden in the text by means of `add-text-properties`.
Here, various info about the structure of the message in
FOLDER-NAME that has UID.

##### mbug-spam

```elisp
(mbug-spam FOLDER-NAME UID)
```

Move the message from FOLDER-NAME to a spam folder.
The Spam folder name is `mbug-spam-folder-name` which can be
customized, the UID is the messsage.

##### mbug-delete

```elisp
(mbug-delete)
```

Mark one more message for deletion ; Press `x` to execute.

##### mbug-move

```elisp
(mbug-move FOLDER-NAME UID TO-FOLDER)
```

From FOLDER-NAME, move UID to TO-FOLDER.

##### mbug-new-mail

```elisp
(mbug-new-mail)
```

Compose a new mail.

##### mbug-logout

```elisp
(mbug-logout)
```

Close the IMAP mail server connection.
Just do anything to open it back again.

##### mbug-undelete

```elisp
(mbug-undelete)
```

Unmark one more condemned message.

##### mbug-expunge

```elisp
(mbug-expunge FOLDER-NAME DOIT)
```

Expunge the current FOLDER-NAME, given DOIT.


___
### mbug-message-mode
The "Message" mode: View and interact with a message.
Available commands in the message composition buffer:

#### mbug-message-mode Keys

Key  | Binding 
------------- | ------------- 
`C-a` | [message-beginning-of-line](#markdown-header-message-beginning-of-line)
`TAB` | [message-tab](#markdown-header-message-tab)
`RET` | [mbug-message-open-attachment](#markdown-header-mbug-message-open-attachment)
`a` | [message-wide-reply](#markdown-header-message-wide-reply)
`h` | [mbug-toggle-headers](#markdown-header-mbug-toggle-headers)
`q` | [mbug-kill-buffer](#markdown-header-mbug-kill-buffer)
`r` | [message-reply](#markdown-header-message-reply)
`C-c C-a` | [mml-attach-file](#markdown-header-mml-attach-file)
`C-c C-b` | [message-goto-body](#markdown-header-message-goto-body)
`C-c C-c` | [message-send-and-exit](#markdown-header-message-send-and-exit)
`C-c C-d` | [message-dont-send](#markdown-header-message-dont-send)
`C-c C-e` | [message-elide-region](#markdown-header-message-elide-region)
`C-c TAB` | [message-goto-signature](#markdown-header-message-goto-signature)
`C-c C-j` | [gnus-delay-article](#markdown-header-gnus-delay-article)
`C-c C-k` | [message-kill-buffer](#markdown-header-message-kill-buffer)
`C-c C-l` | [message-to-list-only](#markdown-header-message-to-list-only)
`C-c C-n` | [message-insert-newsgroups](#markdown-header-message-insert-newsgroups)
`C-c C-o` | [message-sort-headers](#markdown-header-message-sort-headers)
`C-c C-q` | [message-fill-yanked-message](#markdown-header-message-fill-yanked-message)
`C-c C-r` | [message-caesar-buffer-body](#markdown-header-message-caesar-buffer-body)
`C-c C-s` | [message-send](#markdown-header-message-send)
`C-c C-t` | [message-insert-to](#markdown-header-message-insert-to)
`C-c C-u` | [message-insert-or-toggle-importance](#markdown-header-message-insert-or-toggle-importance)
`C-c C-v` | [message-delete-not-region](#markdown-header-message-delete-not-region)
`C-c C-w` | [message-insert-signature](#markdown-header-message-insert-signature)
`C-c C-y` | [message-yank-original](#markdown-header-message-yank-original)
`C-c C-z` | [message-kill-to-signature](#markdown-header-message-kill-to-signature)
`C-c ?` | [describe-mode](#markdown-header-describe-mode)
`M-RET` | [message-newline-and-reformat](#markdown-header-message-newline-and-reformat)
`M-n` | [message-display-abbrev](#markdown-header-message-display-abbrev)
`<remap> <split-line>` | [message-split-line](#markdown-header-message-split-line)
`C-M-i` | [ispell-complete-word](#markdown-header-ispell-complete-word)
`s - i` | [message-insert-or-toggle-importance](#markdown-header-message-insert-or-toggle-importance)
`C-c C-M-y` | [message-yank-buffer](#markdown-header-message-yank-buffer)
`C-c M-f` | [message-mark-insert-file](#markdown-header-message-mark-insert-file)
`C-c M-h` | [message-insert-headers](#markdown-header-message-insert-headers)
`C-c M-k` | [message-kill-address](#markdown-header-message-kill-address)
`C-c M-m` | [message-mark-inserted-region](#markdown-header-message-mark-inserted-region)
`C-c M-n` | [message-insert-disposition-notification-to](#markdown-header-message-insert-disposition-notification-to)
`C-c M-r` | [message-rename-buffer](#markdown-header-message-rename-buffer)
`C-c C-f C-a` | [message-generate-unsubscribed-mail-followup-to](#markdown-header-message-generate-unsubscribed-mail-followup-to)
`C-c C-f C-b` | [message-goto-bcc](#markdown-header-message-goto-bcc)
`C-c C-f C-c` | [message-goto-cc](#markdown-header-message-goto-cc)
`C-c C-f C-d` | [message-goto-distribution](#markdown-header-message-goto-distribution)
`C-c C-f C-e` | [message-insert-expires](#markdown-header-message-insert-expires)
`C-c C-f C-f` | [message-goto-followup-to](#markdown-header-message-goto-followup-to)
`C-c C-f TAB` | [message-insert-or-toggle-importance](#markdown-header-message-insert-or-toggle-importance)
`C-c C-f C-k` | [message-goto-keywords](#markdown-header-message-goto-keywords)
`C-c C-f RET` | [message-goto-mail-followup-to](#markdown-header-message-goto-mail-followup-to)
`C-c C-f C-n` | [message-goto-newsgroups](#markdown-header-message-goto-newsgroups)
`C-c C-f C-o` | [message-goto-from](#markdown-header-message-goto-from)
`C-c C-f C-r` | [message-goto-reply-to](#markdown-header-message-goto-reply-to)
`C-c C-f C-s` | [message-goto-subject](#markdown-header-message-goto-subject)
`C-c C-f C-t` | [message-goto-to](#markdown-header-message-goto-to)
`C-c C-f C-u` | [message-goto-summary](#markdown-header-message-goto-summary)
`C-c C-f C-w` | [message-goto-fcc](#markdown-header-message-goto-fcc)
`C-c C-f a` | [message-add-archive-header](#markdown-header-message-add-archive-header)
`C-c C-f s` | [message-change-subject](#markdown-header-message-change-subject)
`C-c C-f t` | [message-reduce-to-to-cc](#markdown-header-message-reduce-to-to-cc)
`C-c C-f w` | [message-insert-wide-reply](#markdown-header-message-insert-wide-reply)
`C-c C-f x` | [message-cross-post-followup-to](#markdown-header-message-cross-post-followup-to)

#### mbug-message-mode Commands

##### message-beginning-of-line
> Defined in `message.el`


```elisp
(message-beginning-of-line &optional N)
```

Move point to beginning of header value or to beginning of line.
The prefix argument N is passed directly to `beginning-of-line`.

This command is identical to `beginning-of-line` if point is
outside the message header or if the option `message-beginning-of-line`
is nil.

If point is in the message header and on a (non-continued) header
line, move point to the beginning of the header value or the beginning of line,
whichever is closer.  If point is already at beginning of line, move point to
beginning of header value.  Therefore, repeated calls will toggle point
between beginning of field and beginning of line.

##### message-tab
> Defined in `message.el`


```elisp
(message-tab)
```

Complete names according to `message-completion-alist`.
Execute function specified by `message-tab-body-function` when
not in those headers.  If that variable is nil, indent with the
regular text mode tabbing command.

##### mbug-message-open-attachment

```elisp
(mbug-message-open-attachment)
```

Open message attachment.

##### message-wide-reply
> Defined in `message.el`


```elisp
(message-wide-reply &optional TO-ADDRESS)
```

Make a "wide" reply to the message in the current buffer.

##### mbug-toggle-headers

```elisp
(mbug-toggle-headers)
```

Toggle headers.

##### mbug-kill-buffer

```elisp
(mbug-kill-buffer)
```

Kill the buffer.

##### message-reply
> Defined in `message.el`


```elisp
(message-reply &optional TO-ADDRESS WIDE SWITCH-FUNCTION)
```

:around advice: `ad-Advice-message-reply`

Start editing a reply to the article in the current buffer.

##### mml-attach-file
> Defined in `mml.el`


```elisp
(mml-attach-file FILE &optional TYPE DESCRIPTION DISPOSITION)
```

Attach a file to the outgoing MIME message.
The file is not inserted or encoded until you send the message with
`M-x message-send-and-exit' or `M-x message-send' in Message mode,
or `M-x mail-send-and-exit' or `M-x mail-send' in Mail mode.

FILE is the name of the file to attach.  TYPE is its
content-type, a string of the form "type/subtype".  DESCRIPTION
is a one-line description of the attachment.  The DISPOSITION
specifies how the attachment is intended to be displayed.  It can
be either "inline" (displayed automatically within the message
body) or "attachment" (separate from the body).

##### message-goto-body
> Defined in `message.el`


```elisp
(message-goto-body)
```

Move point to the beginning of the message body.

##### message-send-and-exit
> Defined in `message.el`


```elisp
(message-send-and-exit &optional ARG)
```

Send message like `message-send`, then, if no errors, exit from mail buffer.
The usage of ARG is defined by the instance that called Message.
It should typically alter the sending method in some way or other.

##### message-dont-send
> Defined in `message.el`


```elisp
(message-dont-send)
```

Don't send the message you have been editing.
Instead, just auto-save the buffer and then bury it.

##### message-elide-region
> Defined in `message.el`


```elisp
(message-elide-region B E)
```

Elide the text in the region.
An ellipsis (from `message-elide-ellipsis`) will be inserted where the
text was killed.

##### message-goto-signature
> Defined in `message.el`


```elisp
(message-goto-signature)
```

Move point to the beginning of the message signature.
If there is no signature in the article, go to the end and
return nil.

##### gnus-delay-article
> Defined in `gnus-delay.el`


[Arg list not available until function definition is loaded.]

Not documented.

##### message-kill-buffer
> Defined in `message.el`


```elisp
(message-kill-buffer)
```

Kill the current buffer.

##### message-to-list-only
> Defined in `message.el`


```elisp
(message-to-list-only)
```

Send a message to the list only.
Remove all addresses but the list address from To and Cc headers.

##### message-insert-newsgroups
> Defined in `message.el`


```elisp
(message-insert-newsgroups)
```

Insert the Newsgroups header from the article being replied to.

##### message-sort-headers
> Defined in `message.el`


```elisp
(message-sort-headers)
```

Sort the headers of the current message according to `message-header-format-alist`.

##### message-fill-yanked-message
> Defined in `message.el`


```elisp
(message-fill-yanked-message &optional JUSTIFYP)
```

Fill the paragraphs of a message yanked into this one.
Numeric argument means justify as well.

##### message-caesar-buffer-body
> Defined in `message.el`


```elisp
(message-caesar-buffer-body &optional ROTNUM WIDE)
```

Caesar rotate all letters in the current buffer by 13 places.
Used to encode/decode possibly offensive messages (commonly in rec.humor).
With prefix arg, specifies the number of places to rotate each letter forward.
Mail and USENET news headers are not rotated unless WIDE is non-nil.

##### message-send
> Defined in `message.el`


```elisp
(message-send &optional ARG)
```

Send the message in the current buffer.
If `message-interactive` is non-nil, wait for success indication or
error messages, and inform user.
Otherwise any failure is reported in a message back to the user from
the mailer.
The usage of ARG is defined by the instance that called Message.
It should typically alter the sending method in some way or other.

##### message-insert-to
> Defined in `message.el`


```elisp
(message-insert-to &optional FORCE)
```

Insert a To header that points to the author of the article being replied to.
If the original author requested not to be sent mail, don't insert unless the
prefix FORCE is given.

##### message-insert-or-toggle-importance
> Defined in `message.el`


```elisp
(message-insert-or-toggle-importance)
```

Insert a "Importance: high" header, or cycle through the header values.
The three allowed values according to RFC 1327 are `high`, `normal`
and `low`.

##### message-delete-not-region
> Defined in `message.el`


```elisp
(message-delete-not-region BEG END)
```

Delete everything in the body of the current message outside of the region.

##### message-insert-signature
> Defined in `message.el`


```elisp
(message-insert-signature &optional FORCE)
```

Insert a signature.  See documentation for variable `message-signature`.

##### message-yank-original
> Defined in `message.el`


```elisp
(message-yank-original &optional ARG)
```

Insert the message being replied to, if any.
Puts point before the text and mark after.
Normally indents each nonblank line ARG spaces (default 3).  However,
if `message-yank-prefix` is non-nil, insert that prefix on each line.

This function uses `message-cite-function` to do the actual citing.

Just C-u as argument means don't indent, insert no
prefix, and don't delete any headers.

##### message-kill-to-signature
> Defined in `message.el`


```elisp
(message-kill-to-signature &optional ARG)
```

Kill all text up to the signature.
If a numeric argument or prefix arg is given, leave that number
of lines before the signature intact.

##### describe-mode
> Defined in `help.el`


It is bound to C-h m, <help> m, <menu-bar> <help-menu> <describe>
<describe-mode>.

```elisp
(describe-mode &optional BUFFER)
```

Display documentation of current major mode and minor modes.
A brief summary of the minor modes comes first, followed by the
major mode description.  This is followed by detailed
descriptions of the minor modes, each on a separate page.

For this to work correctly for a minor mode, the mode's indicator
variable (listed in `minor-mode-alist`) must also be a function
whose documentation describes the minor mode.

If called from Lisp with a non-nil BUFFER argument, display
documentation for the major and minor modes of that buffer.

##### message-newline-and-reformat
> Defined in `message.el`


```elisp
(message-newline-and-reformat &optional ARG NOT-BREAK)
```

Insert four newlines, and then reformat if inside quoted text.
Prefix arg means justify as well.

##### message-display-abbrev
> Defined in `message.el`


```elisp
(message-display-abbrev &optional CHOOSE)
```

Display the next possible abbrev for the text before point.

##### message-split-line
> Defined in `message.el`


```elisp
(message-split-line)
```

Split current line, moving portion beyond point vertically down.
If the current line has `message-yank-prefix`, insert it on the new line.

##### ispell-complete-word
> Defined in `ispell.el`


It is bound to <menu-bar> <tools> <spell> <ispell-complete-word>.

```elisp
(ispell-complete-word &optional INTERIOR-FRAG)
```

Try to complete the word before or under point.
If optional INTERIOR-FRAG is non-nil then the word may be a character
sequence inside of a word.

Standard ispell choices are then available.

##### message-insert-or-toggle-importance
> Defined in `message.el`


```elisp
(message-insert-or-toggle-importance)
```

Insert a "Importance: high" header, or cycle through the header values.
The three allowed values according to RFC 1327 are `high`, `normal`
and `low`.

##### message-yank-buffer
> Defined in `message.el`


```elisp
(message-yank-buffer BUFFER)
```

Insert BUFFER into the current buffer and quote it.

##### message-mark-insert-file
> Defined in `message.el`


```elisp
(message-mark-insert-file FILE &optional VERBATIM)
```

Insert FILE at point, marking it with enclosing tags.
See `message-mark-insert-begin` and `message-mark-insert-end`.
If VERBATIM, use slrn style verbatim marks ("#v+" and "#v-").

##### message-insert-headers
> Defined in `message.el`


```elisp
(message-insert-headers)
```

Generate the headers for the article.

##### message-kill-address
> Defined in `message.el`


```elisp
(message-kill-address)
```

Kill the address under point.

##### message-mark-inserted-region
> Defined in `message.el`


```elisp
(message-mark-inserted-region BEG END &optional VERBATIM)
```

Mark some region in the current article with enclosing tags.
See `message-mark-insert-begin` and `message-mark-insert-end`.
If VERBATIM, use slrn style verbatim marks ("#v+" and "#v-").

##### message-insert-disposition-notification-to
> Defined in `message.el`


```elisp
(message-insert-disposition-notification-to)
```

Request a disposition notification (return receipt) to this message.
Note that this should not be used in newsgroups.

##### message-rename-buffer
> Defined in `message.el`


```elisp
(message-rename-buffer &optional ENTER-STRING)
```

Rename the *message* buffer to "*message* RECIPIENT".
If the function is run with a prefix, it will ask for a new buffer
name, rather than giving an automatic name.

##### message-generate-unsubscribed-mail-followup-to
> Defined in `message.el`


```elisp
(message-generate-unsubscribed-mail-followup-to &optional INCLUDE-CC)
```

Insert a reasonable MFT header in a post to an unsubscribed list.
When making original posts to a mailing list you are not subscribed to,
you have to type in a MFT header by hand.  The contents, usually, are
the addresses of the list and your own address.  This function inserts
such a header automatically.  It fetches the contents of the To: header
in the current mail buffer, and appends the current `user-mail-address`.

If the optional argument INCLUDE-CC is non-nil, the addresses in the
Cc: header are also put into the MFT.

##### message-goto-bcc
> Defined in `message.el`


```elisp
(message-goto-bcc)
```

Move point to the Bcc  header.

##### message-goto-cc
> Defined in `message.el`


```elisp
(message-goto-cc)
```

Move point to the Cc header.

##### message-goto-distribution
> Defined in `message.el`


```elisp
(message-goto-distribution)
```

Move point to the Distribution header.

##### message-insert-expires
> Defined in `message.el`


```elisp
(message-insert-expires DAYS)
```

Insert the Expires header.  Expiry in DAYS days.

##### message-goto-followup-to
> Defined in `message.el`


```elisp
(message-goto-followup-to)
```

Move point to the Followup-To header.

##### message-insert-or-toggle-importance
> Defined in `message.el`


```elisp
(message-insert-or-toggle-importance)
```

Insert a "Importance: high" header, or cycle through the header values.
The three allowed values according to RFC 1327 are `high`, `normal`
and `low`.

##### message-goto-keywords
> Defined in `message.el`


```elisp
(message-goto-keywords)
```

Move point to the Keywords header.

##### message-goto-mail-followup-to
> Defined in `message.el`


```elisp
(message-goto-mail-followup-to)
```

Move point to the Mail-Followup-To header.

##### message-goto-newsgroups
> Defined in `message.el`


```elisp
(message-goto-newsgroups)
```

Move point to the Newsgroups header.

##### message-goto-from
> Defined in `message.el`


```elisp
(message-goto-from)
```

Move point to the From header.

##### message-goto-reply-to
> Defined in `message.el`


```elisp
(message-goto-reply-to)
```

Move point to the Reply-To header.

##### message-goto-subject
> Defined in `message.el`


```elisp
(message-goto-subject)
```

Move point to the Subject header.

##### message-goto-to
> Defined in `message.el`


```elisp
(message-goto-to)
```

Move point to the To header.

##### message-goto-summary
> Defined in `message.el`


```elisp
(message-goto-summary)
```

Move point to the Summary header.

##### message-goto-fcc
> Defined in `message.el`


```elisp
(message-goto-fcc)
```

Move point to the Fcc header.

##### message-add-archive-header
> Defined in `message.el`


```elisp
(message-add-archive-header)
```

Insert "X-No-Archive: Yes" in the header and a note in the body.
The note can be customized using `message-archive-note`.  When called with a
prefix argument, ask for a text to insert.  If you don't want the note in the
body, set  `message-archive-note` to nil.

##### message-change-subject
> Defined in `message.el`


```elisp
(message-change-subject NEW-SUBJECT)
```

Ask for NEW-SUBJECT header, append (was: <Old Subject>).

##### message-reduce-to-to-cc
> Defined in `message.el`


```elisp
(message-reduce-to-to-cc)
```

Replace contents of To: header with contents of Cc: or Bcc: header.

##### message-insert-wide-reply
> Defined in `message.el`


```elisp
(message-insert-wide-reply)
```

Insert To and Cc headers as if you were doing a wide reply.

##### message-cross-post-followup-to
> Defined in `message.el`


```elisp
(message-cross-post-followup-to TARGET-GROUP)
```

Crossposts message and set Followup-To to TARGET-GROUP.
With prefix-argument just set Follow-Up, don't cross-post.


___
*README.md made on 2017-09-08 at 14:57:45 with [doc-a-mode](https://bitbucket.org/yassinphilip/doc-a-mode)*